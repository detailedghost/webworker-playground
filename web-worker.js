//* Used for multithreading

let times = 0;
const interval = setInterval(() => {
  const message = "Howdy!";
  console.log("%c Sending from webworker: " + message, "color: green");
  self.postMessage(message);
  times++;
  if (times > 10) {
    clearInterval(interval);
    self.close();
  }
}, 1000);
