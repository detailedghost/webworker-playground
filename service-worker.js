//* Used for caching
const CURRENT_VERSION = "v1";

self.addEventListener("install", (e) => {
  e.waitUntil(
    caches
      .open(CURRENT_VERSION)
      .then((cache) => {
        return cache.addAll([
          "index.html",
          "main.js",
          "http://dummy.restapiexample.com/api/v1/employees",
        ]);
      })
      .then(() => self.skipWaiting())
  );
});

self.addEventListener("activate", (event) => {
  const KEEP_LIST = [CURRENT_VERSION];
  // Clearing old cache
  event.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(
        keyList.map((k) =>
          KEEP_LIST.indexOf(k) < 0
            ? caches.delete(k)
            : new Promise((res) => res())
        )
      );
    })
  );
});

//* Used to load resources, and cache calls
self.addEventListener("fetch", (evt) => {
  evt.respondWith(
    //* If request matches cache, return cached
    //* Otherwise, fire off new response and cache that
    caches.open(CURRENT_VERSION).then((cache) =>
      cache.match(evt.request).then((res) => {
        if (res) {
          console.log("Using cache!");
          return res;
        }
        console.log("nothing found, using regular requests");
        return fetch(evt.request)
          .then(async (response) => {
            const clone = response.clone();
            const cache = await caches.open(CURRENT_VERSION);
            cache.put(evt.request, clone);
            return response;
          })
          .catch(() => {
            return new Response("Nothing Found", {
              headers: { "Content-Type": "text/html" },
            });
          });
      })
    )
  );
});

if ("BackgroundFetchManager" in self) {
  console.log("I have background fetch manager!");
}
