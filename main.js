if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker
      .register("./service-worker.js", { scope: "testScope" })
      .then((reg) => {
        console.log("Registration succeeded with scope");
      })
      .catch((err) => {
        console.log("Registration failed: " + err);
      });
  });
}

function createItem(name) {
  const item = document.createElement("li");
  item.innerText = name;
  return item;
}

fetch("http://dummy.restapiexample.com/api/v1/employees")
  .then((r) => r.json())
  .then((r) => {
    const list = document.getElementById("MessageList");
    r.data.forEach((d) => {
      list.appendChild(createItem(d.employee_name));
    });
  });

if("Worker" in window){
  const webWorker = new Worker("web-worker.js");
  webWorker.addEventListener("message", (payload) => {
    console.log("%c Recieving on browser: " + payload.data, "color: lightblue");
  });
}
