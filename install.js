if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./service-worker.js", { scope: "testScope" })
    .then((reg) => {
      console.log("Registration succeeded with scope " + reg.scope);
    })
    .catch((err) => {
      console.log("Registration failed: " + err);
    });
}
